// Declaración de variables para rastrear el contenido actual y el total de contenidos.
var contenidoActual = 1;
var totalContenidos = 3;

// Esperar a que el contenido HTML se cargue completamente.
document.addEventListener("DOMContentLoaded", function() {
    // Llamar a la función para mostrar el contenido actual.
    mostrarContenido();

    // Obtener referencias a los botones "Anterior" y "Siguiente" por su id.
    var botonAnterior = document.getElementById("anterior");
    var botonSiguiente = document.getElementById("siguiente");

    // Agregar un evento de clic al botón "Anterior" para mostrar el contenido anterior.
    botonAnterior.addEventListener("click", function() {
        mostrarContenido('anterior');
    });

    // Agregar un evento de clic al botón "Siguiente" para mostrar el contenido siguiente.
    botonSiguiente.addEventListener("click", function() {
        mostrarContenido('siguiente');
    });
});

// Función para mostrar el contenido actual o navegar hacia adelante/atrás.
function mostrarContenido(direccion) {
    // Obtener una referencia al elemento de contenido actual por su id.
    var contenido = document.getElementById("contenido" + contenidoActual);
    
    // Ocultar el contenido actual al establecer su estilo de visualización en "none".
    contenido.style.display = "none";

    // Determinar la dirección en la que se desea navegar (anterior o siguiente).
    if (direccion === 'anterior') {
        contenidoActual = (contenidoActual - 1) <= 0 ? totalContenidos : contenidoActual - 1;
    } else {
        contenidoActual = (contenidoActual + 1) > totalContenidos ? 1 : contenidoActual + 1;
    }

    // Obtener una referencia al nuevo contenido actual.
    contenido = document.getElementById("contenido" + contenidoActual);

    // Mostrar el nuevo contenido actual estableciendo su estilo de visualización en "block".
    contenido.style.display = "block";
}
