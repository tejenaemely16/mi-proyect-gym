// Esperar a que el contenido HTML se cargue completamente antes de ejecutar el código.
document.addEventListener("DOMContentLoaded", function () {
    // Obtener referencias a elementos HTML relevantes por su ID.
    const inicioSesionForm = document.getElementById("inicio-sesion-form");
    const message = document.getElementById("message");
    const regresarIndexButton = document.getElementById("regresar-index");
    const regresarRegistroButton = document.getElementById("regresar-registro");
    const regresarAdminButton = document.getElementById("regresar-admin");

    // Agregar un manejador de eventos para el formulario de inicio de sesión.
    inicioSesionForm.addEventListener("submit", function (e) {
        e.preventDefault(); // Prevenir el envío del formulario por defecto.
        
        // Obtener el nombre de usuario y contraseña ingresados por el usuario.
        const username = document.getElementById("username").value;
        const password = document.getElementById("password").value;

        // Obtener los usuarios almacenados en el almacenamiento local (localStorage).
        const usuarios = JSON.parse(localStorage.getItem("usuarios")) || [];

        // Validar las credenciales del usuario.
        const usuarioEncontrado = usuarios.find(user => user.username === username && user.password === password);

        if (usuarioEncontrado) {
            showMessage("Inicio de sesión exitoso.", "success");
            // Redirigir al usuario a la página de perfil en caso de éxito.
            window.location.href = "perfil.html";
        } else {
            showMessage("Credenciales incorrectas. Por favor, inténtelo nuevamente.", "error");
        }
    });

    // Función para mostrar mensajes al usuario en la página.
    function showMessage(text, type) {
        message.textContent = text;
        message.className = type === "error" ? "error" : "success";
        if (type === "success") {
            alert("Inicio de sesión exitoso.");
        } else if (type === "error") {
            alert("Credenciales incorrectas. Por favor, inténtelo nuevamente.");
        }
    }

    // Agregar manejadores de eventos para los botones de redirección.
    regresarIndexButton.addEventListener("click", function () {
        window.location.href = "index.html";
    });
    regresarRegistroButton.addEventListener("click", function () {
        window.location.href = "registro.html";
    });
    regresarAdminButton.addEventListener("click", function () {
        window.location.href = "login2.html";
    });
});
