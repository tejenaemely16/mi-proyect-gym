// Esperar a que el contenido HTML se cargue completamente
document.addEventListener("DOMContentLoaded", function () {
    // Obtener una referencia al elemento con el id "lista-servicios"
    const listaServicios = document.getElementById("lista-servicios");

    // Obtener los servicios almacenados en localStorage o crear un arreglo vacío si no hay ninguno
    const servicios = JSON.parse(localStorage.getItem("servicios")) || [];

    // Comprobar si hay servicios registrados
    if (servicios.length === 0) {
        // Mostrar un mensaje si no hay servicios registrados
        listaServicios.innerHTML = "<p>No se han registrado ningún plan.</p>";
    } else {
        // Recorrer la lista de servicios y crear elementos de lista para cada uno
        servicios.forEach(function (servicio, index) {
            // Crear un elemento de lista (li) para mostrar la información del servicio
            const li = document.createElement("li");
            li.innerHTML = `<strong>Plan activo #${index + 1}:</strong> ${servicio.seleccion}<br><strong>Fecha y hora en la que se realizará el pago:</strong> ${servicio.valor}<br/><strong>Duración del plan:</strong> ${servicio.meses} meses<br>`;
            
            // Agregar un botón para eliminar el servicio
            const eliminarButton = document.createElement("button");
            eliminarButton.textContent = "Eliminar";
            eliminarButton.addEventListener("click", function () {
                eliminarServicio(index);
            });

            // Agregar el botón de eliminar al elemento de lista
            li.appendChild(eliminarButton);

            // Agregar el elemento de lista a la lista de servicios
            listaServicios.appendChild(li);
        });
    }

    // Manejar el evento de hacer clic en el botón "Regresar al registro"
    document.getElementById("regresar-registro").addEventListener("click", function () {
        // Redirigir al usuario de vuelta a la página de registro de servicios
        window.location.href = "inscripciones.html";
    });

    // Función para eliminar un servicio de la lista
    function eliminarServicio(index) {
        // Eliminar el servicio del array de servicios
        servicios.splice(index, 1);
        
        // Actualizar los datos en localStorage
        localStorage.setItem("servicios", JSON.stringify(servicios));
        
        // Recargar la página para reflejar los cambios
        window.location.reload();
    }

    // Manejar el evento de hacer clic en el botón "Regresar a la página de inicio"
    document.getElementById("regresarIndexButton").addEventListener("click", function () {
        // Redirigir al usuario a la página de inicio
        window.location.href = "index.html";
    });
});
