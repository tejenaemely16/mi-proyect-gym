// Esperar a que el contenido HTML se cargue completamente antes de ejecutar el código.
document.addEventListener("DOMContentLoaded", function () {
    // Obtener una referencia al formulario de registro por su ID.
    const registroForm = document.getElementById("registro-form");

    // Obtener referencias a botones de redirección por su ID.
    const regresarIndexButton = document.getElementById("regresar-index");
    const regresarInicioSesionButton = document.getElementById("regresar-inicio-sesion");

    // Agregar un manejador de eventos para el formulario de registro.
    registroForm.addEventListener("submit", function (e) {
        e.preventDefault(); // Prevenir el envío del formulario por defecto.

        // Obtener los valores ingresados por el usuario en los campos del formulario.
        const username = document.getElementById("username").value;
        const nombres = document.getElementById("nombres").value;
        const apellidos = document.getElementById("apellidos").value;
        const correo = document.getElementById("correo").value;
        const celular = document.getElementById("celular").value;
        const edad = document.getElementById("edad").value;
        const password = document.getElementById("password").value;
        const confirmPassword = document.getElementById("confirm-password").value;

        // Validaciones de los campos del formulario.
        if (!username || !nombres || !apellidos || !correo || !celular || !edad || !password || !confirmPassword) {
            showAlert("Por favor, complete todos los campos.", "error");
            return;
        }

        if (!/^[a-zA-ZÁÉÍÓÚÜáéíóúü\s]+$/.test(nombres) || !/^[a-zA-ZÁÉÍÓÚÜáéíóúü\s]+$/.test(apellidos)) {
            showAlert("Los campos 'nombres' y 'apellidos' solo pueden contener letras, espacios y caracteres acentuados.", "error");
            return;
        }

        if (password.length < 3) {
            showAlert("La contraseña debe tener al menos 3 caracteres.", "error");
            return;
        }

        if (password !== confirmPassword) {
            showAlert("Las contraseñas no coinciden.", "error");
            return;
        }

        if (isNaN(edad) || edad < 1 || edad > 99) {
            showAlert("La edad debe ser un número entre 1 y 99.", "error");
            return;
        }

        // Validación de dirección de correo electrónico utilizando una expresión regular.
        const correoVali = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
        if (!correoVali.test(correo)) {
            showAlert("Ingrese una dirección de correo electrónico válida.", "error");
            return;
        }

        // Validación del número de teléfono utilizando una expresión regular.
        const celularVali = /^\d{10}$/;
        if (!celularVali.test(celular)) {
            showAlert("Ingrese un número de teléfono válido (10 dígitos).", "error");
            return;
        }

        if (username.length < 3 || username.length > 20) {
            showAlert("El nombre de usuario debe tener entre 3 y 20 caracteres.", "error");
            return;
        }

        // Validación del nombre de usuario utilizando una expresión regular.
        const usuarioVali = /^[a-zA-Z0-9_]+$/;
        if (!usuarioVali.test(username)) {
            showAlert("El nombre de usuario solo puede contener letras, números y guiones bajos.", "error");
            return;
        }

        // Crear un objeto de usuario con los datos ingresados.
        const usuario = {
            username,
            nombres,
            apellidos,
            correo,
            celular,
            edad,
            password
        };

        // Sobrescribir la lista de usuarios con el nuevo usuario en el almacenamiento local (localStorage).
        const usuarios = [usuario];
        localStorage.setItem("usuarios", JSON.stringify(usuarios));

        // Mostrar un mensaje de éxito y restablecer el formulario.
        showAlert("Registro exitoso.", "success");
        registroForm.reset();
    });

    // Función para mostrar mensajes de alerta con diferentes tipos.
    function showAlert(text, type) {
        if (type === "error") {
            alert("Error: " + text);
        } else if (type === "success") {
            alert("Éxito: " + text);
        }
    }

    // Agregar manejadores de eventos para los botones de redirección.
    regresarIndexButton.addEventListener("click", function () {
        window.location.href = "index.html";
    });

    regresarInicioSesionButton.addEventListener("click", function () {
        window.location.href = "login.html";
    });
});
