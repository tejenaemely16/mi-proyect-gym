// Función para actualizar la lista de textos
function actualizarLista() {
    var listaTextos = document.getElementById("listaTextos");
    listaTextos.innerHTML = "";

    // Obtener los textos registrados desde localStorage
    var textosRegistrados = JSON.parse(localStorage.getItem("textosRegistrados")) || [];

    // Iterar a través del arreglo y mostrar cada texto
    for (var i = 0; i < textosRegistrados.length; i++) {
        var servicioMensaje = "Plan " + (i + 1) + ": "; // Mensaje con el número de servicio
        var texto = textosRegistrados[i];
        var listItem = document.createElement("li");
        listItem.textContent = servicioMensaje + texto;

        listaTextos.appendChild(listItem);
    }
}

// Cargar la lista de textos al cargar la página
window.onload = actualizarLista;
