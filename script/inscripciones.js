// Esperar a que el contenido HTML se cargue completamente antes de ejecutar el código.
document.addEventListener("DOMContentLoaded", function () {
    // Obtener referencias a elementos HTML relevantes por su ID.
    const registroServiciosForm = document.getElementById("registro-servicios-form");
    const opcionesServicio = document.getElementById("opciones-servicio");
    const fechaPago = document.getElementById("fecha-pago");
    const horaPago = document.getElementById("hora-pago");
    const meses = document.getElementById("meses");

    // Agregar un manejador de eventos para el formulario de registro de servicios.
    registroServiciosForm.addEventListener("submit", function (e) {
        e.preventDefault(); // Prevenir el envío del formulario por defecto.

        // Obtener los valores ingresados por el usuario.
        const servicioSeleccionado = opcionesServicio.value;
        const fechaSeleccionada = fechaPago.value;
        const horaSeleccionada = horaPago.value;
        const mesesSeleccionados = meses.value;

        // Validar que se haya seleccionado un servicio y una duración válida.
        if (servicioSeleccionado === "seleccionar" || mesesSeleccionados === "seleccionar") {
            alert("Por favor, seleccione un servicio y una duración válidos.");
            return;
        }

        // Combina la fecha y la hora en un solo campo si es necesario.
        const valorSeleccionado = fechaSeleccionada + " " + horaSeleccionada;

        // Obtener los servicios almacenados en el almacenamiento local (localStorage).
        const servicios = JSON.parse(localStorage.getItem("servicios")) || [];

        // Crear un objeto de servicio con los datos ingresados.
        const servicio = {
            seleccion: servicioSeleccionado,
            valor: valorSeleccionado,
            meses: mesesSeleccionados
        };

        // Agregar el servicio a la lista de servicios.
        servicios.push(servicio);
        localStorage.setItem("servicios", JSON.stringify(servicios));

        // Mostrar un mensaje de éxito y reiniciar el formulario.
        alert("Servicio registrado con éxito.");
        registroServiciosForm.reset();
    });

    // Agregar un manejador de eventos para el botón "Ver Servicios".
    document.getElementById("ver-servicios").addEventListener("click", function () {
        // Redirigir al usuario a la página de mostrar servicios.
        window.location.href = "activos.html";
    });

    // Agregar un manejador de eventos para el botón "Regresar al Perfil".
    document.getElementById("regresarPerfilButton").addEventListener("click", function () {
        // Redirigir al usuario a la página de perfil.
        window.location.href = "perfil.html";
    });
});
