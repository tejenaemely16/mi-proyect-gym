// Esperar a que el contenido HTML se cargue completamente antes de ejecutar el código.
document.addEventListener("DOMContentLoaded", function () {
    // Obtener una referencia al botón de "Regresar a Usuario" por su ID.
    const regresarUsuarioButton = document.getElementById("regresar-usuario");

    // Función para validar el formulario de inicio de sesión.
    function validarFormulario() {
        // Obtener los valores ingresados en los campos de nombre de usuario y contraseña.
        var useradminInput = document.getElementById("useradmin").value;
        var passwordInput = document.getElementById("password").value;

        // Validar los datos de inicio de sesión.
        if (useradminInput === "admin" && passwordInput === "123") {
            alert("Inicio de sesión exitoso.");
            // Redirigir al usuario a la página de administrador en caso de éxito.
            window.location.href = "administrador.html";
            return false; // Evita que el formulario se envíe
        } else {
            alert("Usuario o contraseña incorrectos. Por favor, inténtelo de nuevo.");
            return false; // Evita que el formulario se envíe
        }
    }

    // Agregar un manejador de eventos al botón "Regresar a Usuario" para redirigir al usuario.
    regresarUsuarioButton.addEventListener("click", function () {
        window.location.href = "login.html";
    });

    // Obtener una referencia al formulario de inicio de sesión por su ID.
    const loginForm = document.getElementById("loginForm");

    // Agregar un manejador de eventos al formulario para prevenir el envío automático y validar los datos.
    loginForm.addEventListener("submit", function (event) {
        event.preventDefault(); // Evita que el formulario se envíe automáticamente.
        validarFormulario();
    });
});
