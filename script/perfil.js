// Esperar a que el contenido HTML se cargue completamente antes de ejecutar el código.
document.addEventListener("DOMContentLoaded", function () {
    // Obtener una referencia al elemento de perfil de usuario por su ID.
    const userProfile = document.getElementById("user-profile");

    // Obtener una referencia al botón "Cerrar Sesión" por su ID.
    const cerrarSesionButton = document.getElementById("cerrar-sesion");

    // Obtener los datos del usuario desde localStorage. En este caso, asumimos que solo hay un usuario registrado (índice 0).
    const usuario = JSON.parse(localStorage.getItem("usuarios"))[0];

    if (usuario) {
        // Construir la estructura HTML para mostrar los datos del usuario.
        const profileHTML = `
            <p><strong>Nombre de Usuario:</strong> ${usuario.username}</p>
            <p><strong>Nombres:</strong> ${usuario.nombres}</p>
            <p><strong>Apellidos:</strong> ${usuario.apellidos}</p>
            <p><strong>Correo Electrónico:</strong> ${usuario.correo}</p>
            <p><strong>Número de Celular:</strong> ${usuario.celular}</p>
            <p><strong>Edad:</strong> ${usuario.edad}</p>
        `;

        // Insertar la estructura HTML en el elemento de perfil de usuario.
        userProfile.innerHTML = profileHTML;
    }

    // Agregar un manejador de eventos al botón "Cerrar Sesión" para redirigir al usuario a la página de inicio de sesión al cerrar sesión.
    cerrarSesionButton.addEventListener("click", function () {
        window.location.href = "login.html";
    });
});
