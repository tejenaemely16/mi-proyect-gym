// Función para registrar texto
function registrarTexto() {
    var textoInput = document.getElementById("texto").value;

    // Obtener los textos registrados almacenados en localStorage
    var textosRegistrados = JSON.parse(localStorage.getItem("textosRegistrados")) || [];

    // Agregar el nuevo texto al arreglo
    textosRegistrados.push(textoInput);

    // Guardar el arreglo actualizado en localStorage
    localStorage.setItem("textosRegistrados", JSON.stringify(textosRegistrados));

    // Limpiar el campo de texto
    document.getElementById("texto").value = "";

    // Actualizar la lista de textos
    actualizarLista();
}

// Función para actualizar la lista de textos
function actualizarLista() {
    var listaTextos = document.getElementById("listaTextos");
    listaTextos.innerHTML = "";

    // Obtener los textos registrados desde localStorage
    var textosRegistrados = JSON.parse(localStorage.getItem("textosRegistrados")) || [];

    // Iterar a través del arreglo y mostrar cada texto
    for (var i = 0; i < textosRegistrados.length; i++) {
        var texto = textosRegistrados[i];
        var listItem = document.createElement("li");
        listItem.textContent = texto;

        // Agregar un botón para eliminar el texto usando una función de cierre
        var deleteButton = document.createElement("button");
        deleteButton.textContent = "Eliminar";
        deleteButton.onclick = (function(index) {
            return function() {
                eliminarTexto(index);
            };
        })(i);

        listItem.appendChild(deleteButton);
        listaTextos.appendChild(listItem);
    }
}

// Función para eliminar texto
function eliminarTexto(index) {
    // Obtener los textos registrados desde localStorage
    var textosRegistrados = JSON.parse(localStorage.getItem("textosRegistrados")) || [];

    // Eliminar el texto correspondiente
    textosRegistrados.splice(index, 1);

    // Guardar el arreglo actualizado en localStorage
    localStorage.setItem("textosRegistrados", JSON.stringify(textosRegistrados));

    // Actualizar la lista de textos
    actualizarLista();
}

// Cargar la lista de textos al cargar la página
window.onload = actualizarLista;